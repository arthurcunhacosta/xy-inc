CREATE DATABASE XYINC;

CREATE TABLE XYINC.POI (
  ID INT NOT NULL AUTO_INCREMENT,
  NAME VARCHAR(255) NOT NULL,
  COORDX INT NOT NULL,
  COORDY VARCHAR(45) NOT NULL,
  PRIMARY KEY (ID));
