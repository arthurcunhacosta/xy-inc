Projeto feito em Java1.8, utilizando o tomcat 8.0 como servidor e mysql como banco de dados. É possível utilizar o maven para fazer o deploy.

O script do banco se encontra na pasta 'scriptbd', é o arquivo 'scriptbd.sql'. Essa mesma pasta contem o arquivo inserts.sql que possui os inserts com os dados do exemplo do teste

Quando iniciar o tomcat acessar: http://localhost:8080/xyinc para rodar o teste de desenvolvedor.

Algumas observações.
Devido ao tempo que tinha para fazer o teste, algumas coisas que eu gostaria de ter feito não deram tempo. São elas:
1) Validar melhor as entradas dos inputs
2) Fazer alguns testes unitários usando o JUnit
3) Criar uma classe para validar as coordenadas inseridas afim de não existir códido duplicado
4) Melhor e MUITO o meu front-end

