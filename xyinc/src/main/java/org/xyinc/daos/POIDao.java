package org.xyinc.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.xyinc.models.POI;

/**
 * Classe de repositório.
 * Exclusiva para as chamadas no banco de dados.
 */
@Repository
public class POIDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	public void save(POI poi){
		manager.persist(poi);
	}
	
	public void update(POI poi){
		manager.merge(poi);
	}
	
	public List<POI> list() {
		List<POI> list=  manager.createQuery("select distinct(p) from POI p",POI.class).getResultList();
		return list;
	}
	
	public POI findById(Integer id){
		POI poi = manager.createQuery("select distinct(p) from POI p where ID = " + id, POI.class).getSingleResult();
		return poi;
	}

	public void delete(POI poi){
		manager.remove(poi);
	}
}
