package org.xyinc.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Classe modelo para Pontos de interesse.
 * Mapeado na tabela POI do banco XYINC
 * @author arthur
 *
 */
@Entity
@Table(name="POI")
public class POI {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "NAME")
    @NotEmpty(message = "Nome deve ser preenchido!")
	private String name;
	
	@Column(name = "COORDX")
	private Integer coordx;
	
	@Column(name = "COORDY")
	private Integer coordy;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCoordx() {
		return coordx;
	}

	public void setCoordx(Integer coordx) {
		this.coordx = coordx;
	}

	public Integer getCoordy() {
		return coordy;
	}

	public void setCoordy(Integer coordy) {
		this.coordy = coordy;
	}
	
	public boolean validateCoordX(){
		if(null!= this.coordx && this.coordx >=0){
			return true;
		} else{
			return false;
		}
	}
	
	public boolean validateCoordY(){
		if(null!= this.coordy && this.coordy >=0){
			return true;
		} else{
			return false;
		}
	}
}
