package org.xyinc.models;

public class Proximidade {
	
	private Integer coordx;
	private Integer coordy;
	private Integer distance;
	
	public Integer getCoordx() {
		return coordx;
	}
	public void setCoordx(Integer coordx) {
		this.coordx = coordx;
	}
	public Integer getCoordy() {
		return coordy;
	}
	public void setCoordy(Integer coordy) {
		this.coordy = coordy;
	}
	public Integer getDistance() {
		return distance;
	}
	public void setDistance(Integer distance) {
		this.distance = distance;
	}
	
	public boolean validateCoordX(){
		if(null!= this.coordx && this.coordx >=0){
			return true;
		} else{
			return false;
		}
	}
	
	public boolean validateCoordY(){
		if(null!= this.coordy && this.coordy >=0){
			return true;
		} else{
			return false;
		}
	}
	
	public boolean validateDistance(){
		if(null!= this.distance && this.distance >=0){
			return true;
		} else{
			return false;
		}
	}
}
