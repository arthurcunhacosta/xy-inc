package org.xyinc.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.xyinc.controllers.POIController;
import org.xyinc.daos.POIDao;

/**
 * Clasase de configuração
 * Mapeando as views
 * @author arthur
 *
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackageClasses={POIController.class, POIDao.class})
public class AppWebConfiguration {
	
	@Bean
	public InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver resolver =	new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
}
