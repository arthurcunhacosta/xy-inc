package org.xyinc.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.xyinc.daos.POIDao;
import org.xyinc.models.POI;
import org.xyinc.models.Proximidade;

/**
 * Classe para fazer controle dos pontos de interesse.
 * Listar, Salvar, Editar, Deletar e Buscar Pontos de Intesse por proximidade
 * @author arthur
 *
 */
@RestController
@Transactional
@RequestMapping("/")
public class POIController {
	
	@Autowired
	private POIDao poiDao;
	
	/**
	 * Chamando pagina inicial
	 */
	@RequestMapping(value = "/", method=RequestMethod.GET)
	public ModelAndView index(){
		return new ModelAndView("/index")
				.addObject("pois", poiDao.list());
	}
	
	/**
	 * Chamando pagina para adicionar um ponto de interesse
	 */
	@RequestMapping("/add")
	public ModelAndView add(){
		return new ModelAndView("/add");
	}
	
	/**
	 * Chamando pagina para editar um ponto de interesse
	 */
	@RequestMapping("/{id}/edit")
    public ModelAndView edit(@PathVariable Integer id) {
    	return new ModelAndView("/edit").addObject("poi", poiDao.findById(id));
    }
	
	/**
	 * Chamando pagina para deletar um ponto de interesse
	 */
	@RequestMapping(value = "/{id}/delete")
	public ModelAndView delete(@PathVariable Integer id) {
		poiDao.delete(poiDao.findById(id));
		
		return index().addObject("mensagens", "Removido com sucesso");
	}
	
	/**
	 * Chamando pagina para listar um ponto de interesse por proximdade
	 */
	@RequestMapping("/prox")
	public ModelAndView prox() {
		return new ModelAndView("/prox");
	}
	
	/**
	 * Método para salvar um novo ponto de interesse
	 */
	@RequestMapping(value = "/save", method=RequestMethod.POST)
	public ModelAndView save(@Valid @ModelAttribute POI poi, BindingResult result){
		if (!poi.validateCoordX()) {
    		result.addError(new ObjectError("coordx","Coordenada X deve ser maior ou igual a zero."));
    	}
		if (!poi.validateCoordY()) {
    		result.addError(new ObjectError("coordy","Coordenada Y deve ser maior ou igual a zero."));
    	}
		if(result.hasErrors()) {
	            return add()
			    		.addObject("poi", poi)
			    		.addObject("errors", result.getAllErrors());
	        }
		poiDao.save(poi);
		return index().addObject("mensagens", "Inserido com sucesso");
	}

	/**
	 * Método para editar um ponto de interesse
	 */
	@RequestMapping(value = "/update", method=RequestMethod.POST)
    public ModelAndView update(@Valid @ModelAttribute POI poi, BindingResult result) {
		if (!poi.validateCoordX()) {
    		result.addError(new ObjectError("coordx","Coordenada X deve ser maior ou igual a zero."));
    	}
		if (!poi.validateCoordY()) {
    		result.addError(new ObjectError("coordy","Coordenada Y deve ser maior ou igual a zero."));
    	}
		if(result.hasErrors()) {
	            return edit(poi.getId())
			    		.addObject("poi", poi)
			    		.addObject("errors", result.getAllErrors());
	    }
		poiDao.update(poi);
		return index().addObject("mensagens", "Editado com sucesso");
	}
	
	/**
	 * Método para busca um ponto de interesse por proximidade
	 */
	@RequestMapping(value = "/proximity")
	public ModelAndView proximity(@Valid @ModelAttribute Proximidade proximity, BindingResult result) {
		if (!proximity.validateCoordX()) {
    		result.addError(new ObjectError("coordx","Coordenada X deve ser maior ou igual a zero."));
    	}
		if (!proximity.validateCoordY()) {
    		result.addError(new ObjectError("coordy","Coordenada Y deve ser maior ou igual a zero."));
    	}
		if (!proximity.validateDistance()) {
    		result.addError(new ObjectError("distance","Distância deve ser maior ou igual a zero."));
    	}
		if(result.hasErrors()) {
			return prox()
					.addObject("proximity", proximity)
					.addObject("errors", result.getAllErrors());
		 }
		
		List<POI> list = poiDao.list();
		List<POI> listProximity = new ArrayList<>();
		for (POI poi : list) {
			if(pointDistance(proximity.getCoordx(), poi.getCoordx())<=proximity.getDistance() 
					&& pointDistance(proximity.getCoordy(), poi.getCoordy())<=proximity.getDistance()){
				listProximity.add(poi);
			}
		}
		return index().addObject("pois", listProximity);
	}
	
	/**
	 * Método auxiliar para calcular a distância entre dois pontos
	 * @param coordenada
	 * @param coordena
	 * @return distancia entre os pontos
	 */
	private Integer pointDistance(int first, int second){
		if(first>second)
			return first - second;
		else return second - first;
	}
}
