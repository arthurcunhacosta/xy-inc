<nav class="navbar navbar-default">
	<div class="container-fluid">
		<ul class="nav navbar-nav">
 			<li class="active"><a href="${pageContext.request.contextPath}">Listar Pontos de Interesse</a></li>
 			<li class="active"><a href="${pageContext.request.contextPath}/add">Adicionar Pontos de Interesse</a></li>
   			<li class="active"><a href="${pageContext.request.contextPath}/prox">Listar por proximidade</a></li>
  		</ul>
	</div>
</nav>