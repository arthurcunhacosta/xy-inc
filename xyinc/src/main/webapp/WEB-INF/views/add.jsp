<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>XY Inc. | Arthur Costa</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
	</head>
	
	<body>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		
		<div id="nav">
				<jsp:include page="nav.jsp" />
		</div>
		
		<h2>Adicionar Coordenada</h2>
		<form:form method="post" action="/xyinc/save" commandName="poi">
			<jsp:include page="form.jsp" />
		</form:form>
		<div class="col s12 m12 l4">
			<c:if test="${not empty errors}">
				<c:forEach items="${errors}" var="error">
					<div class="row">
						<div class="card-panel red darken-4 white-text">
							${error.defaultMessage}
						</div>
					</div>
				</c:forEach>
			</c:if>
		</div>
	</body>
</html>
