<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<c:choose>
	<c:when test="${empty pois}">
		<div">
			<spam>Nenhum registro encontrado</spam>
		</div>
	</c:when>
	<c:otherwise>
		<table class="table">
			<tr>
				<td>Nome</td>
				<td>Coordenada X</td>
				<td>Coordenada Y</td>
				<td>Ações</td>
			</tr>
			<c:forEach items="${pois}" var="poi">
				<tr>
					<td>${poi.name}</td>
					<td>${poi.coordx}</td>
					<td>${poi.coordy}</td>
					<td><a href="${pageContext.request.contextPath}/${poi.id}/edit">Editar</a>  | <a href="${pageContext.request.contextPath}/${poi.id}/delete">Excluir</a> </td>
				</tr>
			</c:forEach>
		</table>
	</c:otherwise>
</c:choose>