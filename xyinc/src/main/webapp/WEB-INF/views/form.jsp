<input type="hidden" name="id" value="${poi.id}"/>
<div class="form-group">
	<label for="title">Nome</label>
	<input type="text" name="name" id="name" value="${poi.name}" class="form-control">
</div>
<div class="form-group">
	<label for="description">Coordenada X</label>
	<input type="text" name="coordx" id="coordx" value="${poi.coordx}" class="form-control"/>
</div>
<div class="form-group">
 	<label for="pages">Coordenada Y</label>
 	<input type="text" name="coordy" id="coordy" value="${poi.coordy}" class="form-control"/>
</div>
<div>
	<input type="submit" class="btn btn-primary" value="Enviar">
</div>
